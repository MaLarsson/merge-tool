# Main CMakeLists.txt
cmake_minimum_required(VERSION 3.5.1)
project(merge-tool)


# Set constants
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)


# Adds source files to SRCS
macro (add_sources)
  file (RELATIVE_PATH _rel "${PROJECT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
  foreach (_src ${ARGN})
    if (_rel)
      list (APPEND SRCS "${_rel}/${_src}")
    else()
      list (APPEND SRCS "${_src}")
    endif()
  endforeach()
  if (_rel)
    set (SRCS ${SRCS} PARENT_SCOPE)
  endif()
endmacro()


# Add sub directories
add_subdirectory(src)


# Make executable
add_executable(merge-tool ${SRCS})


# Compiler warnings
if (MSVC)
  target_compile_options(merge-tool PRIVATE "/W4")
else()
  target_compile_options(merge-tool PRIVATE "-pedantic")
  target_compile_options(merge-tool PRIVATE "-Wall")
  target_compile_options(merge-tool PRIVATE "-Wextra")
endif()


# Find Qt5 stuff
find_package(Qt5 COMPONENTS Widgets REQUIRED)
include_directories(${CMAKE_BINARY_DIR})
target_link_libraries(merge-tool Qt5::Widgets)
