#include "merge_window.h"
#include <QApplication>
#include <string>


int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    MergeWindow win;
    win.show();

    return app.exec();
}
