#include "merge_window.h"
#include "ui_merge_window.h"

#include "tree_model.h"

using std::make_unique;


MergeWindow::MergeWindow(QWidget* parent, Qt::WindowFlags f)
    : QDialog(parent, f), ui_(make_unique<Ui::MergeWindow>())
{
    ui_->setupUi(this);

    auto model = new TreeModel({"test", "dsa"});
    ui_->sourceTree->setModel(model);
}


MergeWindow::~MergeWindow() = default;
