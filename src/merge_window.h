#ifndef MERGE_WINDOW_H
#define MERGE_WINDOW_H

#pragma once

#include <memory>
#include <QDialog>

namespace Ui {
class MergeWindow;
}


class MergeWindow : public QDialog
{
    Q_OBJECT

 public:
    MergeWindow(QWidget* parent=nullptr, Qt::WindowFlags f=0);
    ~MergeWindow();

 private:
    std::unique_ptr<Ui::MergeWindow> ui_;
};


#endif /// MERGE_WINDOW_H
