#include "tree_model.h"

using std::vector;
using std::string;
using std::make_unique;


TreeModel::TreeModel(const vector<string>& headers, const string& data,
		     QObject* parent)
    : QAbstractItemModel(parent)
{
    /**
     * test code ...
     * TODO: change to xml import ...
     */

    rootNode_ = make_unique<TreeNode>(headers);

    rootNode_->appendChild(make_unique<TreeNode>(headers, rootNode_.get()));
    rootNode_->appendChild(make_unique<TreeNode>(headers, rootNode_.get()));
    rootNode_->appendChild(make_unique<TreeNode>(headers, rootNode_.get()));

    auto node = rootNode_->
	appendChild(make_unique<TreeNode>(headers, rootNode_.get()));

    node->appendChild(make_unique<TreeNode>(headers, node));
    node->appendChild(make_unique<TreeNode>(headers, node));

    rootNode_->appendChild(make_unique<TreeNode>(headers, rootNode_.get()));
}


QVariant TreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
	return QVariant();
    }

    if (role != Qt::DisplayRole && role != Qt::EditRole) {
        return QVariant();
    }

    return QVariant(getNode(index)->data(index.column()).c_str());;
}


QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
			       int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        return QVariant(rootNode_->data(section).c_str());
    }

    return QVariant();
}


QModelIndex TreeModel::index(int row, int column,
			     const QModelIndex& parent) const
{
    if (!parent.isValid() || parent.column() == 0) {
	auto childNode = getNode(parent)->child(row);

	if (childNode) {
	    return createIndex(row, column, childNode);
	}
    }

    return QModelIndex();
}


QModelIndex TreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return QModelIndex();
    }

    auto parentNode = getNode(index)->parent();

    if (parentNode == rootNode_.get()) {
        return QModelIndex();
    }

    return createIndex(parentNode->childNumber(), 0, parentNode);
}


int TreeModel::rowCount(const QModelIndex& parent) const
{
    return getNode(parent)->childCount();
}


int TreeModel::columnCount(const QModelIndex&) const
{
    return rootNode_->columnCount();
}


TreeNode* TreeModel::getNode(const QModelIndex& index) const
{
    if (index.isValid()) {
	auto item = static_cast<TreeNode*>(index.internalPointer());

        if (item) { return item; }
    }

    return rootNode_.get();
}
