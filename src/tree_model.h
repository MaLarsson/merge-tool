#ifndef TREE_MODEL_H
#define TREE_MODEL_H

#pragma once

#include "tree_node.h"
#include <QAbstractItemModel>


class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

 public:
    TreeModel(const std::vector<std::string>& headers,
	      const std::string& data = "", QObject* parent=nullptr);

    ~TreeModel() = default;

    QVariant data(const QModelIndex& index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
			int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column,
		      const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

 private:
    TreeNode* getNode(const QModelIndex& index) const;

    std::unique_ptr<TreeNode> rootNode_;
};


#endif /// TREE_MODEL_H
