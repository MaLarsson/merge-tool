#include "tree_node.h"
#include <algorithm>

using std::string;
using std::out_of_range;
using std::find_if;
using std::distance;
using std::unique_ptr;
using std::make_unique;
using std::vector;
using std::move;


TreeNode* TreeNode::parent()
{
    return parent_;
}


TreeNode* TreeNode::child(int number)
{
    try {
	return children_.at(number).get();
    }
    catch (const out_of_range& e) {
	return nullptr;
    }
}


int TreeNode::childCount() const
{
    return children_.size();
}


int TreeNode::columnCount() const
{
    return data_.size();
}


string TreeNode::data(int column) const
{
    try {
	return data_.at(column);
    }
    catch (const out_of_range& e) {
	return "";
    }
}

bool TreeNode::setData(int column, const string& value)
{
    try {
	data_.at(column) = value;
	return true;
    }
    catch (const out_of_range& e) {
	return false;
    }
}


TreeNode* TreeNode::appendChild(unique_ptr<TreeNode>&& child)
{
    auto ptr = child.get();

    children_.push_back(move(child));

    return ptr;
}


TreeNode* TreeNode::appendChild(const vector<string>& data)
{
    auto elem = make_unique<TreeNode>(data, this);
    auto ptr = elem.get();

    children_.emplace_back(move(elem));

    return ptr;
}


int TreeNode::childNumber() const
{
    if (parent_ != nullptr) {
	auto start = parent_->children_.begin();

	auto pos = find_if(start, parent_->children_.end(),
			   [&](auto& child) { return child.get() == this; });

	return distance(start, pos);
    }

    return 0;
}
