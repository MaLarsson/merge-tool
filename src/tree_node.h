#ifndef TREE_NODE_H
#define TREE_NODE_H

#pragma once

#include <memory>
#include <vector>
#include <string>


class TreeNode
{
 public:
    explicit TreeNode(const std::vector<std::string>& data,
		      TreeNode* parent=nullptr)
	: data_(data), parent_(parent)
    {}

    ~TreeNode() = default;

    TreeNode* parent();
    TreeNode* child(int number);

    int childCount() const;
    int columnCount() const;

    std::string data(int column) const;
    bool setData(int column, const std::string& value);

    TreeNode* appendChild(std::unique_ptr<TreeNode>&& child);
    TreeNode* appendChild(const std::vector<std::string>& data);

    int childNumber() const;

 private:
    std::vector<std::string> data_;

    TreeNode* parent_;
    std::vector<std::unique_ptr<TreeNode>> children_;
};


#endif /// TREE_NODE_H
